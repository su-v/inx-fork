#!/usr/bin/env python
'''
This extension scales a document to fit different SVG DPI -90/96-

Copyright (C) 2012 Jabiertxo Arraiza, jabier.arraiza@marker.es
Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

Version 0.7 - DPI Switcher

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Changes since v0.5:
    - transform all top-level containers and graphics elements
    - support scientific notation in SVG lengths
    - fix scaling with existing matrix() (use functions from simpletransform.py)
    - support different units for document width, height attributes
    - improve viewBox support (syntax, offset)
    - support common cases of text-put-on-path in SVG root
    - support common cases of <use> references in SVG root
    - examples from http://tavmjong.free.fr/INKSCAPE/UNITS/ tested
Changes since v0.6:
    - scale perspective of 3dboxes
    - grids, guides supported for up- and downscaling
    - extended document scale information
    - support common cases of linked offset references in SVG root

TODO:
    - check <symbol> instances
    - check more <use> and text-on-path cases (reverse scaling needed?)
    - support preserveAspectRatio attribute (if viewBox is present)

'''
# pylint: disable=too-many-lines

# standard libraries
import re
import math
from lxml import etree
# local libraries
import inkex
import simpletransform
import simplestyle


# globals
ABSOLUTE_UNITS = ('pt', 'pc', 'cm', 'mm', 'in')
PIXEL_UNITS = ('', 'px')
RELATIVE_UNITS = ('em', 'ex', '%')
SKIP_CONTAINERS = [
    'defs',
    'glyph',
    'marker',
    'mask',
    'missing-glyph',
    'pattern',
    'symbol',
]
CONTAINER_ELEMENTS = [
    'a',
    'g',
    'switch',
]
GRAPHICS_ELEMENTS = [
    'circle',
    'ellipse',
    'image',
    'line',
    'path',
    'polygon',
    'polyline',
    'rect',
    'text',
    'use',
]


# ----- helper functions for transforms

def format_scale(fx, fy):
    """Format scale() transformation with fractions."""
    return 'scale({},{})'.format(float(fx), float(fy))


def normalize_pt2(point):
    """Normalize coordinates of projected 3d point."""
    if math.fabs(point[2]) < 1E-6 or point[2] == 1.0:
        return point
    point[0] /= point[2]
    point[1] /= point[2]
    point[2] = 1.0
    return point


# ----- helper functions for SVG lengths and coordinates

def check_svglength_unit(unit):
    """Check whether unit is valid for SVG lengths."""
    return (unit in ABSOLUTE_UNITS or
            unit in PIXEL_UNITS or
            unit in RELATIVE_UNITS)


def parse_length(val):
    """Parse length into list of float (value) and string (unit)."""
    value = 0.0
    unit = ''
    if isinstance(val, str):
        split_val = re.match(
            r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)(.*$)', val).groups()
        if len(split_val):
            value = float(split_val[0])
        if len(split_val) > 1:
            unit = split_val[-1].strip()
    return [value, unit]


def print_length(alist):
    """Return length string."""
    return '{}{}'.format(*alist)


def parse_point(val):
    """Parse point string into list of floats."""
    # pylint: disable=bad-builtin
    return [float(v) for v in filter(None, re.split("[, ]", val))]


def print_point(alist):
    """Return point list as string joined with comma."""
    return ','.join(str(v) for v in alist)


def parse_list_of_lengths(val):
    """Parse list-of-lengths string into list of lengths."""
    # pylint: disable=bad-builtin
    return [parse_length(v) for v in filter(None, re.split("[, ]+", val))]


def print_list_of_lengths(alist):
    """Return list-of-lengths list as string joined with whitespace."""
    return ' '.join(print_length(l) for l in alist)


def parse_list_of_numbers(val):
    # pylint: disable=bad-builtin
    """Parse list-of-numbers string into list of floats."""
    return [float(v) for v in filter(None, re.split("[, ]+", val))]


def print_list_of_numbers(alist):
    """Return list-of-numbers list as string joined with whitespace."""
    return ' '.join(str(v) for v in alist)


def parse_list_of_3dcoords(val):
    """Parse 3dpoint string into list of floats."""
    # pylint: disable=bad-builtin
    return [float(v) for v in filter(None, re.split("[:]", val))]


def print_list_of_3dcoords(alist):
    """Return 3dpoint list as string joined with whitespace."""
    return ' : '.join(str(v) for v in alist)


# ----- helper functions for processing SVG elements

def is_perspective(element):
    """Check whether element is an Inkscape 3dbox perspective."""
    return (element.tag == inkex.addNS('perspective', 'inkscape') and
            element.get(inkex.addNS('type', 'sodipodi')) == 'inkscape:persp3d')


def is_3dbox(element):
    """Check whether element is an Inkscape 3dbox type."""
    return (element.tag == inkex.addNS('g', 'svg') and
            element.get(inkex.addNS('type', 'sodipodi')) == 'inkscape:box3d')


def is_3dbox_side(element):
    """Check if element is box3dsidetype."""
    return (element.tag == inkex.addNS('path', 'svg') and
            element.get(
                inkex.addNS('type', 'sodipodi')) == 'inkscape:box3dside')


def is_use(element):
    """Check whether element is of type <text>."""
    return element.tag == inkex.addNS('use', 'svg')


def is_text(element):
    """Check whether element is of type <text>."""
    return element.tag == inkex.addNS('text', 'svg')


def is_text_on_path(element):
    """Check whether text element is put on a path."""
    if is_text(element):
        text_path = element.find(inkex.addNS('textPath', 'svg'))
        if text_path is not None:
            return True
    return False


def is_offset(element):
    """Check whether path element is a offset."""
    return (element.tag == inkex.addNS('path', 'svg') and
            element.get(inkex.addNS('type', 'sodipodi')) == 'inkscape:offset')


def is_linked_offset(element):
    """Check whether path element is a linked offset."""
    return is_offset(element) and inkex.addNS('href', 'xlink') in element.attrib


def is_sibling(element1, element2):
    """Check whether element1 and element2 are siblings of same parent."""
    return element2 in element1.getparent()


def is_in_defs(doc, element):
    """Check whether element is in defs."""
    if element is not None:
        defs = doc.find('svg:defs', namespaces=inkex.NSS)
        if defs is not None:
            return element in defs.iterdescendants()
    return False


def get_viewbox(node):
    """Return viewBox of node if set."""
    attr = 'viewBox'
    viewbox = []
    if attr in node.attrib:
        viewbox = parse_list_of_numbers(node.get(attr))
    return viewbox


def get_width(node):
    """Return width of node."""
    attr = 'width'
    width = [None, None]
    if attr in node.attrib:
        width = parse_length(node.get(attr))
    return width


def get_height(node):
    """Return height of node."""
    attr = 'height'
    height = [None, None]
    if attr in node.attrib:
        height = parse_length(node.get(attr))
    return height


def get_linked(doc, element):
    """Return linked element or None."""
    if element is not None:
        href = element.get(inkex.addNS('href', 'xlink'), None)
    if href is not None:
        linked_id = href[href.find('#')+1:]
        path = '//*[@id="%s"]' % linked_id
        el_list = doc.xpath(path, namespaces=inkex.NSS)
        if isinstance(el_list, list) and len(el_list):
            return el_list[0]
        else:
            return None


def get_perspectives(node):
    """Return all inkscape perspective in node."""
    xpath_str = '//inkscape:perspective'
    return node.xpath(xpath_str, namespaces=inkex.NSS)


def get_guides(node):
    """Return all sodipodi guides in node."""
    xpath_str = '//sodipodi:guide'
    return node.xpath(xpath_str, namespaces=inkex.NSS)


def get_grids(node):
    """Return all inkscape grids in node."""
    xpath_str = '//inkscape:grid'
    return node.xpath(xpath_str, namespaces=inkex.NSS)


def parse_grid(node):
    """Parse grid attributes."""
    data = {'id': node.get('id'),
            'type': 'xygrid',
            'legacy': False,
            'pixel': False,
            'units': ''}
    grid_type = node.get('type')
    if grid_type == 'xygrid':
        attribs = {'originx': [0.0, ''], 'originy': [0.0, ''],
                   'spacingx': [1.0, ''], 'spacingy': [1.0, '']}
    elif grid_type == 'axonomgrid':
        data['type'] = grid_type
        # FIXME: default values can be absolute (legacy doc) or user units
        # for axonom grid, the default values in the user prefs are 'mm'
        attribs = {'originx': [0.0, 'mm'], 'originy': [0.0, 'mm'],
                   'spacingy': [1.0, 'mm'],
                   'gridanglex': [30.0, ''], 'gridangley': [30.0, '']}
    data['units'] = node.get('units', 'px')
    for attr in attribs.keys():
        # FIXME: default value is computed based on user prefs
        val, unit = attribs[attr]
        if attr in node.attrib:
            val, unit = parse_length(node.get(attr))
        if unit:
            data['legacy'] = unit
            if unit == 'px':
                data['pixel'] = True
        data[attr] = [val, unit]
    return data


# ----- scale attributes of elements

def scale_attr_val(element, attr, unit_list, factor):
    """Scale attribute value if unit matches one in *unit_list*."""
    if attr in element.attrib:
        val, unit = parse_length(element.get(attr))
        if unit in unit_list:
            element.set(attr, '{}{}'.format(val * factor, unit))


def scale_list_of_lengths(element, prop, scale):
    """Scale list of lengths for text position shifts."""
    if prop in element.attrib:
        lengths = parse_list_of_lengths(element.get(prop))
        for length in lengths:
            length[0] *= scale
        element.set(prop, print_list_of_lengths(lengths))


def scale_props_length(element, props, scale):
    """Scale length in list of properties of element."""
    sdict = simplestyle.parseStyle(element.get('style'))
    for prop in props:
        if prop in element.attrib:
            val, unit = parse_length(element.get(prop))
            element.set(prop, print_length([val * scale, unit]))
        if prop in sdict:
            val, unit = parse_length(sdict[prop])
            sdict[prop] = print_length([val * scale, unit])
            element.set('style', simplestyle.formatStyle(sdict))


def scale_guide(node, scale_x, scale_y=None):
    """Scale position of guide."""
    scale_y = scale_x if scale_y is None else scale_y
    # scale attributes
    attr = 'position'
    point = parse_point(node.get(attr))
    point[0] *= scale_x
    point[1] *= scale_y
    node.set(attr, print_point(point))


def scale_xygrid(node, scale_x, scale_y):
    """Scale origin, spacing of rectangular grid."""
    attribs = {'originx': "0.0", 'originy': "0.0",
               'spacingx': "1.0", 'spacingy': "1.0"}
    for attr in attribs:
        if attr in node.attrib:
            val, unit = parse_length(node.get(attr))
        else:
            # FIXME: default value is computed based on user prefs
            val, unit = parse_length(attribs[attr])
        if attr.endswith('x'):
            val *= scale_x
        elif attr.endswith('y'):
            val *= scale_y
        node.set(attr, '{}{}'.format(val, unit))


def scale_axonomgrid(node, scale_x, scale_y):
    """Scale origin, spacing of axonometric grid."""
    attribs = {'originx': "0.0mm", 'originy': "0.0mm", 'spacingy': "1.0mm"}
    # inkex.debug('DPI Switcher: Unhandled grid type!')
    for attr in attribs:
        if attr in node.attrib:
            val, unit = parse_length(node.get(attr))
        else:
            # FIXME: default value is computed based on user prefs
            val, unit = parse_length(attribs[attr])
        if attr.endswith('x'):
            val *= scale_x
        elif attr.endswith('y'):
            val *= scale_y
        node.set(attr, '{}{}'.format(val, unit))


def scale_grid(node, doc_unit, doc_scale, scale_x, scale_y=None):
    """Scale attributes of inkscape grid."""
    # pylint: disable=too-many-branches
    scale_y = scale_x if scale_y is None else scale_y
    # init variables
    need_fix_guides = True
    need_fix_grid_mm = doc_unit in ABSOLUTE_UNITS  # TODO: true?
    did_scaling = True  # TODO: always true?
    # parse grid attributes
    grid_data = parse_grid(node)
    # inkex.debug(grid_data)
    if grid_data['legacy']:
        if grid_data['pixel']:
            if need_fix_grid_mm:
                # force update
                grid_scale_x = 1.0
                grid_scale_y = 1.0
            else:
                grid_scale_x = scale_x / doc_scale[0]
                grid_scale_y = scale_y / doc_scale[1]
        else:  # not pixel grid
            if need_fix_grid_mm:
                grid_scale_x = 1.0 / scale_x
                grid_scale_y = 1.0 / scale_y
            else:
                grid_scale_x = 1.0 / doc_scale[0]
                grid_scale_y = 1.0 / doc_scale[1]
        # scale attributes
        if grid_data['type'] == 'xygrid':
            scale_xygrid(node, grid_scale_x, grid_scale_y)
        elif grid_data['type'] == 'axonomgrid':
            scale_axonomgrid(node, grid_scale_x, grid_scale_y)
    else:  # not legacy grid
        if need_fix_guides:  # unit in ABSOLUTE_UNITS?
            if did_scaling:
                grid_scale_x = scale_x
                grid_scale_y = scale_y
            else:
                # force update
                grid_scale_x = 1.0
                grid_scale_y = 1.0
            # scale attributes
            if grid_data['type'] == 'xygrid':
                scale_xygrid(node, grid_scale_x, grid_scale_y)
            elif grid_data['type'] == 'axonomgrid':
                scale_axonomgrid(node, grid_scale_x, grid_scale_y)


def scale_persp3d_attribs(element, scale_x, scale_y=None):
    """Scale attributes of perspective definition."""
    scale_y = scale_x if scale_y is None else scale_y
    props = ['persp3d-origin', 'vp_x', 'vp_y', 'vp_z']
    for prop in [inkex.addNS(v, 'inkscape') for v in props]:
        if prop in element.attrib:
            pt2 = normalize_pt2(parse_list_of_3dcoords(element.get(prop)))
            pt2[0] *= scale_x
            pt2[1] *= scale_y
            element.set(prop, print_list_of_3dcoords(pt2))


def scale_text_attribs(element, scale_x, scale_y=None):
    """Scale individual font attributes and properties."""
    # NOTE: does not support inline stylesheet element <style />
    scale_y = scale_x if scale_y is None else scale_y
    mat = simpletransform.parseTransform(format_scale(scale_x, scale_y))
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    descrim = math.sqrt(abs(det))
    props = ['font-size', 'letter-spacing', 'word-spacing', 'stroke-width']
    # outer text
    scale_list_of_lengths(element, 'dx', scale_x)
    scale_list_of_lengths(element, 'dy', scale_y)
    scale_props_length(element, props, descrim)
    # inner tspans
    for child in element.iterdescendants():
        if child.tag == inkex.addNS('tspan', 'svg'):
            scale_list_of_lengths(child, 'dx', scale_x)
            scale_list_of_lengths(child, 'dy', scale_y)
            scale_props_length(child, props, descrim)


def scale_use_attribs(element, scale_x, scale_y=None):
    """Scale individual use attributes and properties."""
    # NOTE: does not support inline stylesheet element <style />
    scale_y = scale_x if scale_y is None else scale_y
    mat = simpletransform.parseTransform(format_scale(scale_x, scale_y))
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    descrim = math.sqrt(abs(det))
    props = ['font-size', 'letter-spacing', 'word-spacing', 'stroke-width']
    # use
    scale_props_length(element, props, descrim)


def scale_offset_attribs(element, scale_x, scale_y=None):
    """Scale individual offset attributes and properties."""
    # NOTE: does not support inline stylesheet element <style />
    scale_y = scale_x if scale_y is None else scale_y
    mat = simpletransform.parseTransform(format_scale(scale_x, scale_y))
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    descrim = math.sqrt(abs(det))
    props = ['font-size', 'letter-spacing', 'word-spacing', 'stroke-width']
    # use
    scale_props_length(element, props, descrim)


# ----- check special cases

def check_3dbox(svg, element, scale_x, scale_y):
    """Check transformation for 3dbox element."""
    # pylint: disable=unused-argument
    skip = False  # don't skip: transform will scale style properties
    return skip


def check_text_on_path(svg, element, scale_x, scale_y):
    """Check whether to skip scaling a text put on a path."""
    skip = False
    path = get_linked(svg, element.find(inkex.addNS('textPath', 'svg')))
    if not is_in_defs(svg, path):
        if is_sibling(element, path):
            # skip common element scaling if both text and path are siblings
            skip = True
            # scale offset
            if 'transform' in element.attrib:
                mat = simpletransform.parseTransform(element.get('transform'))
                mat[0][2] *= scale_x
                mat[1][2] *= scale_y
                element.set('transform', simpletransform.formatTransform(mat))
            # scale font properties
            scale_text_attribs(element, scale_x, scale_y)
    return skip


def check_use(svg, element, scale_x, scale_y):
    """Check whether to skip scaling an instantiated element (<use>)."""
    skip = False
    path = get_linked(svg, element)
    if not is_in_defs(svg, path):
        if is_sibling(element, path):
            skip = True
            # scale offset
            if 'transform' in element.attrib:
                mat = simpletransform.parseTransform(element.get('transform'))
                mat[0][2] *= scale_x
                mat[1][2] *= scale_y
                element.set('transform', simpletransform.formatTransform(mat))
            # scale lengths in style properties
            scale_use_attribs(element, scale_x, scale_y)
    return skip


def check_offset(svg, element, scale_x, scale_y):
    """Check whether to skip scaling a linked offset element."""
    skip = False
    path = get_linked(svg, element)
    if not is_in_defs(svg, path):
        if is_sibling(element, path):
            skip = True
            # scale offset
            if 'transform' in element.attrib:
                mat = simpletransform.parseTransform(element.get('transform'))
                mat[0][2] *= scale_x
                mat[1][2] *= scale_y
                element.set('transform', simpletransform.formatTransform(mat))
            # scale lengths in style properties
            scale_offset_attribs(element, scale_x, scale_y)
    return skip


class DPISwitcher(inkex.Effect):
    """inkex.Effect()-based class to switch current document's DPI."""

    def __init__(self):
        inkex.Effect.__init__(self)
        # instance attributes
        self.units = "px"
        # options
        self.OptionParser.add_option("--switcher", action="store",
                                     type="string", dest="switcher", default="0",
                                     help="Select the DPI switch you want")
        self.OptionParser.add_option("--action", action="store",
                                     type="string", dest="action",
                                     default=None, help="")

    # dictionaries of unit to user unit conversion factors
    __uuconv_90dpi = {
        'in': 90.0,
        'pt': 1.25,
        'px': 1.0,
        'mm': 3.5433070866,
        'cm': 35.433070866,
        'm': 3543.3070866,
        'km': 3543307.0866,
        'pc': 15.0,
        'yd': 3240.0,
        'ft': 1080.0,
    }
    __uuconv_96dpi = {
        'in': 96.0,
        'pt': 1.33333333333,
        'px': 1.0,
        'mm': 3.77952755913,
        'cm': 37.7952755913,
        'm': 3779.52755913,
        'km': 3779527.55913,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
    }

    def set_dpiswitcher_version(self, svg):
        """Temporarily update inkscape:version string."""
        inkversion = inkex.addNS('version', 'inkscape')
        version_str = svg.get(inkversion, '')
        name_self = self.__class__.__name__
        if not version_str.startswith(name_self):
            version_str = '{} (before: {})'.format(name_self, version_str)
        svg.set(inkversion, version_str)

    def convert_length(self, val, unit, to_unit=None):
        """Convert length to *to_unit* if unit differs."""
        # default to self.units
        to_unit = self.units if to_unit is None else to_unit
        # only convert between absolute units
        if unit not in RELATIVE_UNITS and to_unit not in RELATIVE_UNITS:
            from_unit = 'px' if not unit else unit
            to_unit = 'px' if not to_unit else to_unit
            if from_unit != to_unit:
                if self.options.switcher == "0":  # dpi90to96
                    known_units = self.__uuconv_90dpi.keys()
                    if from_unit in known_units and to_unit in known_units:
                        val_px = val * self.__uuconv_90dpi[from_unit]
                        val = val_px / (self.__uuconv_90dpi[to_unit] / self.__uuconv_90dpi['px'])
                        unit = to_unit
                else:  # dpi96to90
                    known_units = self.__uuconv_96dpi.keys()
                    if from_unit in known_units and to_unit in known_units:
                        val_px = val * self.__uuconv_96dpi[from_unit]
                        val = val_px / (self.__uuconv_96dpi[to_unit] / self.__uuconv_96dpi['px'])
                        unit = to_unit
        return (val, unit)

    def _get_document_scale(self, node):
        """Return document scale based on viewport size in px and viewBox."""
        # defined in src/document.cpp:606
        # Returns document scale as defined by width/height (in pixels) and
        # viewBox (real world to user-units).
        doc = node.getroottree().getroot()
        scale = [1.0, 1.0]
        viewbox = get_viewbox(doc)
        if len(viewbox) == 4:
            scale_x = 1.0
            scale_y = 1.0
            if viewbox[2] > 0.0:
                width = get_width(doc)
                if len(width) and width[0] is not None:
                    if width[1] not in RELATIVE_UNITS:
                        width_px = self.convert_length(*width, to_unit='px')[0]
                        scale_x = width_px / viewbox[2]
                    else:
                        scale_x = width[0] / 100.0
            if viewbox[3] > 0.0:
                height = get_height(doc)
                if len(height) and height[0] is not None:
                    if height[1] not in RELATIVE_UNITS:
                        height_px = self.convert_length(*height, to_unit='px')[0]
                        scale_y = height_px / viewbox[3]
                    else:
                        scale_y = height[0] / 100.0
            scale = [scale_x, scale_y]
        return scale

    def scale_guides(self, svg, parent, factor):
        """Scale sodipodi guides."""
        if (self.units in PIXEL_UNITS or
                (self.units in ABSOLUTE_UNITS and 'viewBox' in svg.attrib)):
            # inkex.debug('Fixing guides ...')
            for guide in get_guides(parent):
                scale_guide(guide, factor)

    def scale_grids(self, svg, parent, factor):
        """Scale inkscape grids."""
        if self.units not in RELATIVE_UNITS:
            # inkex.debug('Fixing grids ...')
            doc_scale = self._get_document_scale(svg)
            for grid in get_grids(parent):
                scale_grid(grid, self.units, doc_scale, factor)

    def scale_defs(self, parent, factor):
        """Check defs for specific elements to scale."""
        if self.units not in ABSOLUTE_UNITS:
            # inkex.debug('Fixing perspectives ...')
            for persp3d in get_perspectives(parent):
                scale_persp3d_attribs(persp3d, factor)

    def scale_elements(self, svg, dpifactor):
        """Scale top-level elements in SVG root."""
        # pylint: disable=too-many-branches

        for element in svg.iterchildren(tag=inkex.etree.Element):

            tag = etree.QName(element).localname

            if tag == 'defs':
                # scale specific elements in defs
                self.scale_defs(element, dpifactor)
                continue

            if tag == 'namedview':
                # scale specific elements in namedview
                self.scale_guides(svg, element, dpifactor)
                self.scale_grids(svg, element, dpifactor)
                continue

            if tag == 'style':
                # TODO: scale properties in inline style sheet?
                continue

            width_scale = dpifactor
            height_scale = dpifactor

            if tag in GRAPHICS_ELEMENTS or tag in CONTAINER_ELEMENTS:

                # test for specific elements to skip from scaling
                if is_3dbox(element):
                    if check_3dbox(svg, element, width_scale, height_scale):
                        continue
                if is_text_on_path(element):
                    if check_text_on_path(svg, element, width_scale, height_scale):
                        continue
                if is_use(element):
                    if check_use(svg, element, width_scale, height_scale):
                        continue
                if is_linked_offset(element):
                    if check_offset(svg, element, width_scale, height_scale):
                        continue

                # relative units in presentation attributes
                for attr in ['x', 'width']:
                    scale_attr_val(element, attr, RELATIVE_UNITS, 1.0 / width_scale)
                for attr in ['y', 'height']:
                    scale_attr_val(element, attr, RELATIVE_UNITS, 1.0 / height_scale)

                # set preserved transforms on top-level elements
                if width_scale != 1.0 and height_scale != 1.0:
                    mat = simpletransform.parseTransform(format_scale(width_scale, height_scale))
                    simpletransform.applyTransformToNode(mat, element)

    def scale_root(self, svg, dpifactor):
        """Scale SVG root."""

        # init booleans
        need_fix_viewbox = False
        need_fix_units = False

        # get relevant info from SVG document
        viewbox = get_viewbox(svg)
        width, width_unit = get_width(svg)
        height, height_unit = get_height(svg)

        # check viewport units
        for vp_unit in (width_unit, height_unit):
            if vp_unit in ABSOLUTE_UNITS:
                need_fix_viewbox = True
            elif vp_unit in PIXEL_UNITS:
                need_fix_units = True
            elif vp_unit in RELATIVE_UNITS:
                pass
            elif vp_unit is None:
                pass
            else:
                inkex.debug('DPI Switcher: Unhandled viewport unit "{}"!'.format(vp_unit))
                return

        # update viewport
        if need_fix_units:
            self.units = 'px'
            if svg.get('width'):
                svg.set('width', print_length([width * dpifactor, width_unit]))
            if svg.get('height'):
                svg.set('height', print_length([height * dpifactor, height_unit]))

        elif need_fix_viewbox:
            # width and/or height use absolute units, none uses pixel
            self.units = width_unit if width_unit in ABSOLUTE_UNITS else height_unit

        # update viewBox
        if len(viewbox):
            svg.set('viewBox', print_list_of_numbers([(val * dpifactor) for val in viewbox]))

        # update top-level elements
        self.scale_elements(svg, dpifactor)

        # temporary update inkscape version
        self.set_dpiswitcher_version(svg)

    def doc_info_scale_dim(self, label, length, vb_length, display_units):
        """Display scale of viewport dimension."""
        # pylint: disable=too-many-locals, too-many-statements
        show = inkex.debug
        name, axis = label
        # init variables
        css_u = 'px'
        doc_u = 'px'
        dis_u = display_units
        met_u = 'mm'
        uu_css = 1.0
        uu_doc = 1.0
        uu_dis = 1.0
        uu_met = 1.0
        # check length units and convert to display, metric and pixel units
        doc = length
        doc_u = length[1]
        if doc_u not in RELATIVE_UNITS:
            # absolute or pixel units
            css = self.convert_length(*doc, to_unit=css_u)
            dis = self.convert_length(*doc, to_unit=dis_u)
            met = self.convert_length(*doc, to_unit=met_u)
            if vb_length:
                uu_doc = doc[0] / vb_length
                uu_css = css[0] / vb_length
                uu_dis = dis[0] / vb_length
                uu_met = met[0] / vb_length
            else:
                uu_doc = doc[0] / css[0]
                uu_dis = dis[0] / css[0]
                uu_met = met[0] / css[0]
        else:
            # relative units
            if vb_length:
                css = ((doc[0]/100.0) * vb_length, css_u)
                dis = self.convert_length(*css, to_unit=dis_u)
                met = self.convert_length(*css, to_unit=met_u)
                uu_doc = doc[0] / 100.0
                uu_css = css[0] / vb_length
                uu_dis = dis[0] / vb_length
                uu_met = met[0] / vb_length
            else:
                css = (doc[0], css_u)
                dis = self.convert_length(*css, to_unit=dis_u)
                met = self.convert_length(*css, to_unit=met_u)
                uu_doc = doc[0] / css[0]
                uu_dis = dis[0] / css[0]
                uu_met = met[0] / css[0]
        # format output
        show('')
        show('{} Scale ({}):'.format(axis, doc_u or '-'))
        show('Document {} in document units: {:.3f}{}'.format(name, *doc))
        show('Document {} in display units: {:.3f}{}'.format(name, *dis))
        show('Document {} in metric units: {:.3f}{}'.format(name, *met))
        show('Document {} in CSS pixels: {:.3f}{}'.format(name, *css))
        show('1 SVG user unit corresponds to {:.3f}{} (document units).'.format(uu_doc, doc_u))
        show('1 SVG user unit corresponds to {:.3f}{} (display units).'.format(uu_dis, dis_u))
        show('1 SVG user unit corresponds to {:.3f}{} (metric units).'.format(uu_met, met_u))
        show('1 SVG user unit corresponds to {:.3f}{} (CSS Pixels).'.format(uu_css, css_u))

    def doc_info_scale(self, svg, display_units):
        """Display information about document scale."""
        # TODO: if viewBox is present, take preserveAspectRatio into account
        show = inkex.debug

        # svg attributes
        width = get_width(svg)
        height = get_height(svg)
        viewbox = get_viewbox(svg)

        # Store current switcher option (based on INX)
        current_switcher = self.options.switcher

        # DPI of current runtime (best guess based on inkex feature)
        try:
            # inkscape >= 0.91
            ink_dpi = self.uutounit(self.unittouu('1in'), 'px')  # pylint: disable=no-member
        except AttributeError:
            # inkscape <= 0.48
            ink_dpi = inkex.uutounit(inkex.unittouu('1in'), 'px')  # pylint: disable=no-member

        # Switcher (for uuconv) based on inkscape version
        if ink_dpi == 96:
            # inkscape >= 0.92
            self.options.switcher = "1"
        else:
            # inkscape <= 0.91
            self.options.switcher = "0"

        show("\n::: Document scale information ({} DPI) :::".format(int(ink_dpi)))

        if None not in width:
            label = ['width', 'X']
            vb_width = viewbox[2] if len(viewbox) == 4 else 0
            self.doc_info_scale_dim(label, width, vb_width, display_units)
        if None not in height:
            label = ['height', 'Y']
            vb_height = viewbox[3] if len(viewbox) == 4 else 0
            self.doc_info_scale_dim(label, height, vb_height, display_units)

        # restore switcher
        self.options.switcher = current_switcher

    def doc_info(self, svg):
        """Display document information."""
        show = inkex.debug

        # sodipodi, inkscape attributes
        inkversion = inkex.addNS('version', 'inkscape')
        document_units = inkex.addNS('document-units', 'inkscape')
        nv = svg.find(inkex.addNS('namedview', 'sodipodi'))

        show("::: SVG document information :::\n")

        # Inkscape version
        if inkversion in svg.attrib:
            show('Last edited with: Inkscape {}'.format(svg.get(inkversion)))

        # SVG root attributes
        for attr in ['width', 'height', 'viewBox']:
            if attr in svg.attrib:
                show('Document {}: {}'.format(attr, svg.get(attr)))
            else:
                show('Document {} not defined.'.format(attr))

        # Document scale (computed length / viewbox)
        if len(get_viewbox(svg)):
            current_switcher = self.options.switcher
            self.options.switcher = "0"
            # inkex.debug(self.convert_length(1, 'in', to_unit='px'))
            doc_scale = self._get_document_scale(svg)
            show('Document scale' +
                 ' based on {} DPI:'.format(['90', '96'][int(self.options.switcher)]) +
                 ' ({:.3f},{:.3f})'.format(*doc_scale))
            self.options.switcher = "1"
            # inkex.debug(self.convert_length(1, 'in', to_unit='px'))
            doc_scale = self._get_document_scale(svg)
            show('Document scale' +
                 ' based on {} DPI:'.format(['90', '96'][int(self.options.switcher)]) +
                 ' ({:.3f},{:.3f})'.format(*doc_scale))
            self.options.switcher = current_switcher

        # More document scale
        self.doc_info_scale(svg, nv.get(document_units, 'px'))

        show("\n::: Inkscape document properties :::\n")

        # Display units
        if document_units in nv.attrib:
            show('Display units: {}'.format(nv.get(document_units)))

        # guides
        guides = get_guides(nv)
        if len(guides):
            show('Document has {} guide{}.'.format(len(guides), 's' if len(guides) > 1 else ''))

        # grids
        grids = get_grids(nv)
        if len(grids):
            show('Document has {} grid{}.'.format(len(grids), 's' if len(grids) > 1 else ''))
        for i, grid in enumerate(grids, 1):
            grid_data = parse_grid(grid)
            if grid_data['legacy']:
                show('Grid number {} (legacy): Units: {}'.format(i, grid_data['units']))
            else:
                show('Grid number {}: Units: {}'.format(i, grid_data['units']))

    def doc_switch(self, svg):
        """Scale document based on selected dpi switch."""
        # CSS pixels per inch
        dpi90 = 90.0
        dpi96 = 96.0
        # dpi factor
        if self.options.switcher == "0":    # dpi90to96
            ratio = dpi90 / dpi96  # pylint: disable=unused-variable
            dpifactor = 1.0 / (dpi90 / dpi96)
        elif self.options.switcher == "1":  # dpi96to90
            ratio = dpi96 / dpi90  # pylint: disable=unused-variable
            dpifactor = 1.0 / (dpi96 / dpi90)
        else:
            dpifactor = 1.0
        self.scale_root(svg, dpifactor)

    def effect(self):
        """Main entry to display or switch scale of current document."""
        svg = self.document.getroot()
        if self.options.action == '"page_info"':
            self.doc_info(svg)
        else:
            self.doc_switch(svg)


if __name__ == '__main__':
    ME = DPISwitcher()
    ME.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
