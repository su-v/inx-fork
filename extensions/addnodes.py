#!/usr/bin/env python 
'''
This extension either adds nodes to a path so that
    a) no segment is longer than a maximum value 
    or
    b) so that each segment is divided into a given number of equal segments

Copyright (C) 2005,2007 Aaron Spike, aaron@ekips.org
CSP-None conversion by Parcly Taxel (2015), reddeloostw@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
# standard library
import copy
import math
import re

# compat
import six

# local library
import inkex
import cubicsuperpathx as cubicsuperpath
import simplestyle
import bezmisc


def numsegs(csp):
    return sum([len(p)-1 for p in csp])
def tpoint(x1_y1, x2_y2, t = 0.5):
    x1, y1 = x1_y1
    x2, y2 = x2_y2
    return [x1+t*(x2-x1),y1+t*(y2-y1)]
def cspbezsplit(sp1, sp2, t = 0.5):
    m1=tpoint(sp1[1],sp1[2],t)
    m2=tpoint(sp1[2],sp2[0],t)
    m3=tpoint(sp2[0],sp2[1],t)
    m4=tpoint(m1,m2,t)
    m5=tpoint(m2,m3,t)
    m=tpoint(m4,m5,t)
    a = sp1[0][:] if sp1[0] is not None else None
    b = sp2[2][:] if sp2[2] is not None else None
    return [[a,sp1[1][:],m1], [m4,m,m5], [m3,sp2[1][:],b]]
def cspbezsplitatlength(sp1, sp2, l = 0.5, tolerance = 0.001):
    bez = (sp1[1][:],sp1[2][:],sp2[0][:],sp2[1][:])
    t = bezmisc.beziertatlength(bez, l, tolerance)
    return cspbezsplit(sp1, sp2, t)
def cspseglength(sp1,sp2, tolerance = 0.001):
    bez = (sp1[1][:],sp1[2][:],sp2[0][:],sp2[1][:])
    return bezmisc.bezierlength(bez, tolerance)    
def csplength(csp):
    total = 0
    lengths = []
    for sp in csp:
        lengths.append([])
        for i in six.moves.xrange(1,len(sp)):
            l = cspseglength(sp[i-1],sp[i])
            lengths[-1].append(l)
            total += l            
    return lengths, total
def numlengths(csplen):
    retval = 0
    for sp in csplen:
        for l in sp:
            if l > 0:
                retval += 1
    return retval

class SplitIt(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("--segments",
                        action="store", type="int", 
                        dest="segments", default=2,
                        help="Number of segments to divide the path into")
        self.OptionParser.add_option("--maxlen",
                        action="store", type="float",
                        dest="maxlen", default=10.0,
                        help="Maximum segment length")
        self.OptionParser.add_option("--unit",
                        action="store", type="string",
                        dest="unit", default='uu',
                        help="Unit for max. segment length")
        self.OptionParser.add_option("--method",
                        action="store", type="string", 
                        dest="method", default='',
                        help="The kind of division to perform")

    def effect(self):

        if self.options.unit == 'uu':
            maxlen = self.options.maxlen
        else:
            unitfactor = self.unittouu("1{}".format(self.options.unit))
            maxlen = self.options.maxlen * unitfactor

        for node in self.selected.values():
            if node.tag == inkex.addNS('path','svg'):
                p = cubicsuperpath.parsePath(node.get('d'))
                
                #lens, total = csplength(p)
                #avg = total/numlengths(lens)
                #inkex.debug("average segment length: %s" % avg)

                new = []
                for sub in p:
                    newsub = [sub[0][:]]
                    closed = cubicsuperpath.closedness(sub)
                    i = 1
                    while i <= len(sub):
                        j = i % len(sub)
                        if j > 0 or closed > 0:
                            length = cspseglength(newsub[-1], sub[j])

                            if self.options.method == 'bynum':
                                splits = self.options.segments
                            else:
                                splits = math.ceil(length/maxlen)

                            for s in six.moves.xrange(int(splits),1,-1):
                                newsub[-1], nextspt, sub[j] = cspbezsplitatlength(newsub[-1], sub[j], 1.0/s)
                                newsub.append(nextspt[:])
                            newsub.append(sub[j])
                        i += 1
                    if closed > 0:
                        newsub[0][0] = newsub[-1][0][:]
                        del newsub[-1]
                    new.append(newsub)
                node.set('d',cubicsuperpath.formatPath(new))

if __name__ == '__main__':
    e = SplitIt()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
