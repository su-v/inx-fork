#!/usr/bin/env python

# standard library
import string

# local library
import chardataeffect


class C(chardataeffect.CharDataEffect):
  def process_chardata(self,text, line=False, par=False):
    return text.lower()

c = C()
c.affect()
