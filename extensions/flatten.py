#!/usr/bin/env python 
'''
Copyright (C) 2006 Aaron Spike, aaron@ekips.org
CSP-None conversion by Parcly Taxel (2015), reddeloostw@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
# local library
import inkex
import cubicsuperpathx as cubicsuperpath
import simplepath
import cspxsubdiv as cspsubdiv


class MyEffect(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("-f", "--flatness",
                        action="store", type="float", 
                        dest="flat", default=10.0,
                        help="Minimum flatness of the subdivided curves")

    def effect(self):
        for node in self.selected.values():
            if node.tag == inkex.addNS('path','svg'):
                d = node.get('d')
                p = cubicsuperpath.parsePath(d)
                cspsubdiv.cspsubdiv(p, self.options.flat)
                np = []
                for sp in p:
                    tmplist = [['L', csp[1]] for csp in sp]
                    tmplist[0][0] = 'M'
                    closed = cubicsuperpath.closedness(sp)
                    if closed > 0:
                        if closed == 1:
                            tmplist.append(['L', sp[0][1]])
                        tmplist.append(['Z', []])
                    np.extend(tmplist)
                node.set('d',simplepath.formatPath(np))


if __name__ == '__main__':
    e = MyEffect()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
