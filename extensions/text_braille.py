#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard library
import string

# local library
import chardataeffect


class C(chardataeffect.CharDataEffect):

  def process_chardata(self,text, line, par):

    convert_table = {\
      'a': self.to_unicode("⠁", "utf-8"),\
      'b': self.to_unicode("⠃", "utf-8"),\
      'c': self.to_unicode("⠉", "utf-8"),\
      'd': self.to_unicode("⠙", "utf-8"),\
      'e': self.to_unicode("⠑", "utf-8"),\
      'f': self.to_unicode("⠋", "utf-8"),\
      'g': self.to_unicode("⠛", "utf-8"),\
      'h': self.to_unicode("⠓", "utf-8"),\
      'i': self.to_unicode("⠊", "utf-8"),\
      'j': self.to_unicode("⠚", "utf-8"),\
      'k': self.to_unicode("⠅", "utf-8"),\
      'l': self.to_unicode("⠇", "utf-8"),\
      'm': self.to_unicode("⠍", "utf-8"),\
      'n': self.to_unicode("⠝", "utf-8"),\
      'o': self.to_unicode("⠕", "utf-8"),\
      'p': self.to_unicode("⠏", "utf-8"),\
      'q': self.to_unicode("⠟", "utf-8"),\
      'r': self.to_unicode("⠗", "utf-8"),\
      's': self.to_unicode("⠎", "utf-8"),\
      't': self.to_unicode("⠞", "utf-8"),\
      'u': self.to_unicode("⠥", "utf-8"),\
      'v': self.to_unicode("⠧", "utf-8"),\
      'w': self.to_unicode("⠺", "utf-8"),\
      'x': self.to_unicode("⠭", "utf-8"),\
      'y': self.to_unicode("⠽", "utf-8"),\
      'z': self.to_unicode("⠵", "utf-8"),\
    }

    r = ""
    for c in text:
      if c.lower() in convert_table:
        r = r + convert_table[c.lower()]
      else:
        r = r + c
    return r

c = C()
c.affect()
