#!/usr/bin/env python 
'''
This extension converts a path into a dashed line using 'stroke-dasharray'
It is a modification of the file addnodes.py

Copyright (C) 2005,2007 Aaron Spike, aaron@ekips.org
Copyright (C) 2009 Alvin Penner, penner@vaxxine.com
CSP-None conversion by Parcly Taxel (2015), reddeloostw@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
# local library
import inkex
import cubicsuperpathx as cubicsuperpath
import bezmisc
import simplestyle

def tpoint(x1_y1, x2_y2, t = 0.5):
    x1, y1 = x1_y1
    x2, y2 = x2_y2
    return [x1+t*(x2-x1),y1+t*(y2-y1)]
def cspbezsplit(sp1, sp2, t = 0.5):
    m1=tpoint(sp1[1],sp1[2],t)
    m2=tpoint(sp1[2],sp2[0],t)
    m3=tpoint(sp2[0],sp2[1],t)
    m4=tpoint(m1,m2,t)
    m5=tpoint(m2,m3,t)
    m=tpoint(m4,m5,t)
    a = sp1[0][:] if sp1[0] is not None else None
    b = sp2[2][:] if sp2[2] is not None else None
    return [[a,sp1[1][:],m1], [m4,m,m5], [m3,sp2[1][:],b]]
def cspbezsplitatlength(sp1, sp2, l = 0.5, tolerance = 0.001):
    bez = (sp1[1][:],sp1[2][:],sp2[0][:],sp2[1][:])
    t = bezmisc.beziertatlength(bez, l, tolerance)
    return cspbezsplit(sp1, sp2, t)
def cspseglength(sp1,sp2, tolerance = 0.001):
    bez = (sp1[1][:],sp1[2][:],sp2[0][:],sp2[1][:])
    return bezmisc.bezierlength(bez, tolerance)    

class SplitIt(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.not_converted = []

    def effect(self):
        for node in self.selected.values():
            self.convert2dash(node)
        if len(self.not_converted):
            inkex.errormsg(_('Total number of objects not converted:') +
                           ' {}\n'.format(len(self.not_converted)))
            # return list of IDs in case the user needs to find a specific object
            inkex.debug(self.not_converted)

    def convert2dash(self, node):
        if node.tag == inkex.addNS('g', 'svg'):
            for child in node:
                self.convert2dash(child)
        else:
            if node.tag == inkex.addNS('path','svg'):
                dashes = []
                offset = 0
                style = simplestyle.parseStyle(node.get('style'))
                if 'stroke-dasharray' in style:
                    dashes = [float(dash) for dash in style['stroke-dasharray'].split(',')]
                    if len(dashes) % 2: dashes.extend(dashes)
                if 'stroke-dashoffset' in style:
                    offset = style['stroke-dashoffset']
                if dashes:
                    p = cubicsuperpath.parsePath(node.get('d'))
                    new = []
                    for sub in p:
                        newsub = []
                        idash = 0
                        dash = dashes[0]
                        length = float(offset)
                        while dash <= length:
                            length -= dash
                            idash = (idash + 1) % len(dashes)
                            dash = dashes[idash]
                        startrender = idash % 2 == 0
                        newsub.append([sub[0][:]])
                        i = 1
                        while i <= len(sub):
                            j = i % len(sub)
                            dash -= length
                            if j > 0 or cubicsuperpath.closedness(sub) > 0:
                                length = cspseglength(newsub[-1][-1], sub[j])
                                while dash < length:
                                    newsub[-1][-1], nextspt, sub[j] = cspbezsplitatlength(newsub[-1][-1], sub[j], dash/length)
                                    if idash % 2:
                                        # create a gap
                                        newsub[-1][-1][2] = None
                                        newsub.append([[None] + nextspt[1:]])
                                    else:
                                        # splice the curve
                                        newsub[-1].append(nextspt[:])
                                    length -= dash
                                    idash = (idash + 1) % len(dashes)
                                    dash = dashes[idash]
                                if idash % 2:
                                    if j > 0:
                                        newsub.append([sub[j]])
                                else:
                                    newsub[-1].append(sub[j])
                            i += 1
                        if startrender and idash % 2 == 0:
                            # This may close open curves with a solid segment
                            # TODO: verify whether checking closedness is correct
                            if cubicsuperpath.closedness(sub):
                                # Final node joining
                                newsub[-1][-1][2] = newsub[0][0][2][:]
                                newsub[-1].extend(newsub[0][1:])
                                del newsub[0]
                        new.extend(newsub)
                    node.set('d',cubicsuperpath.formatPath(new))
                    del style['stroke-dasharray']
                    node.set('style', simplestyle.formatStyle(style))
                    node.attrib.pop(inkex.addNS('type', 'sodipodi'), None)
                    node.attrib.pop(inkex.addNS('nodetypes', 'sodipodi'), None)
            else:
                self.not_converted.append(node.get('id'))

if __name__ == '__main__':
    e = SplitIt()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
