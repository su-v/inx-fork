#!/usr/bin/env python

# local library
from bezmisc import beziersplitatt
from ffgeom import Point, Segment


def maxdist(curve):
    """Maximal distance of controlpoints p1, p2 to coord (p0 to p3)."""
    p0 = Point(*curve[0])
    p1 = Point(*curve[1])
    p2 = Point(*curve[2])
    p3 = Point(*curve[3])

    s1 = Segment(p0,p3)
    return max(s1.distanceToPoint(p1),s1.distanceToPoint(p2))


def subdiv(sp,flat,i=0):
    """Subdivide a subpath of a CubicSuperPath."""
    while i < len(sp):
        p0 = sp[i-1][1]
        p1 = sp[i-1][2]
        p2 = sp[i][0]
        p3 = sp[i][1]
        if p1 == None and p2 == None:
            i += 1
        else:
            b = (p0,p1,p2,p3)
            m = maxdist(b)
            if m <= flat:
                i += 1
            else:
                one, two = beziersplitatt(b,0.5)
                sp[i-1][2], sp[i][0] = one[1], two[2]
                sp[i:0] = [[one[2],one[3],two[1]]]


def cspsubdiv(csp,flat):
    """Subdivide a CubicSuperPath into straight segments."""
    for sp in csp:
        subdiv(sp,flat)


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
