#!/usr/bin/env python 
'''
dimension.py
An Inkscape effect for adding CAD style dimensions to selected objects
in a drawing.

It uses the selection's bounding box, so if the bounding box has empty
space in the x- or y-direction (such as with some stars) the results
will look strange.  Strokes might also overlap the edge of the 
bounding box.

The dimension arrows aren't measured: use the "Visualize Path/Measure
Path" effect to add measurements.

This code contains snippets from existing effects in the Inkscape
extensions library, and marker data from markers.svg.

Copyright (C) 2007 Peter Lewerin, peter.lewerin@tele2.se

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
# standard library
import locale
import sys
from subprocess import Popen, PIPE

# compat
import six

# local library
import inkex
import pathmodifier
from simpletransform import *


ENCODING = sys.stdin.encoding
if ENCODING == 'cp0' or ENCODING is None:
    ENCODING = locale.getpreferredencoding()


# ---- query objects

def run(command_format, stdin_str=None, verbose=False):
    """Run command"""
    if verbose:
        inkex.debug(command_format)
    out = err = None
    myproc = Popen(command_format, shell=False,
                   stdin=PIPE, stdout=PIPE, stderr=PIPE)
    if sys.version_info > (2,):
        if stdin_str is not None:
            stdin_str = stdin_str.encode()
    out, err = myproc.communicate(stdin_str)
    if sys.version_info > (2,):
        if out is not None:
            out = out.decode(ENCODING)
    if myproc.returncode == 0:
        return out
    elif err is not None:
        inkex.errormsg(err)


def query_bbox(svg_file, obj_id, scale=1.0):
    """Return position, size of visual bbox of element with id *obj_id*.

    The values can be scaled by a single factor (document scale), and
    represent x, y and width, height relative to the coordinate system
    of the document's initial viewport.
    """
    opts = ['inkscape', '--shell']
    stdin_str = ""
    for arg in ['x', 'y', 'width', 'height']:
        stdin_str += '--file="{}" '.format(svg_file)
        stdin_str += '--query-id={} '.format(obj_id)
        stdin_str += '--query-{} '.format(arg)
        stdin_str += '\n'
    stdout_str = run(opts, stdin_str, verbose=False)
    if stdout_str is not None:
        stdout_lst = stdout_str.split('>')
        if len(stdout_lst) >= 5:
            return [scale * float(s) for s in stdout_lst[1:5]]


# ----- draw helper functions

def bbox_default_style():
    """Return default style for visualized bbox shape or path."""
    return "fill:blue;fill-opacity:0.25;stroke:none"


def bbox_extents_to_rect(bbox):
    """Return bbox extents converted to rect data (x, y, w, h)."""
    return [bbox[0], bbox[2], bbox[1] - bbox[0], bbox[3] - bbox[2]]


def draw_bbox_path(bbox):
    """Draw path with bbox position and dimensions."""
    x, y, width, height = bbox_extents_to_rect(bbox)
    path_d = ""
    path_d += "m {},{} ".format(x, y)
    path_d += "h {} ".format(width)
    path_d += "v {} ".format(height)
    path_d += "h {} ".format(-width)
    path_d += "z"
    bbox_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    bbox_path.set('d', path_d)
    bbox_path.set('style', bbox_default_style())
    return bbox_path


def draw_bbox_rect(bbox):
    """Draw rect with bbox position and dimensions."""
    x, y, width, height = bbox_extents_to_rect(bbox)
    bbox_rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    bbox_rect.set('x', str(x))
    bbox_rect.set('y', str(y))
    bbox_rect.set('width', str(width))
    bbox_rect.set('height', str(height))
    bbox_rect.set('style', bbox_default_style())
    return bbox_rect


def draw_bbox(bbox, node):
    """Wrapper for default bbox drawing func."""
    bbox_path = draw_bbox_path(bbox)
    if node.getparent() is not None:
        node.getparent().append(bbox_path)
        applyTransformToNode(rootToNodeTransform(node.getparent()), bbox_path)
    else:
        node.append(bbox_path)


# ----- main class

class Dimension(pathmodifier.PathModifier):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.bbox = None
        self.OptionParser.add_option("-x", "--xoffset",
                        action="store", type="float", 
                        dest="xoffset", default=100.0,
                        help="x offset of the vertical dimension arrow")    
        self.OptionParser.add_option("-y", "--yoffset",
                        action="store", type="float", 
                        dest="yoffset", default=100.0,
                        help="y offset of the horizontal dimension arrow")    
        self.OptionParser.add_option("-t", "--type",
                        action="store", type="string", 
                        dest="type", default="geometric",
                        help="Bounding box type")
        self.OptionParser.add_option("--wrap_in_group",
                        action="store", type="inkbool",
                        dest="wrap_in_group", default=False,
                        help="Wrap objects in group")
        self.OptionParser.add_option("--debug",
                        action="store", type="inkbool",
                        dest="debug", default=True,
                        help="Debug option (visualize bbox)")

    def addMarker(self, name, rotate):
        defs = self.xpathSingle('/svg:svg//svg:defs')
        if defs == None:
            defs = inkex.etree.SubElement(self.document.getroot(),inkex.addNS('defs','svg'))
        marker = inkex.etree.SubElement(defs ,inkex.addNS('marker','svg'))
        marker.set('id', name)
        marker.set('orient', 'auto')
        marker.set('refX', '0.0')
        marker.set('refY', '0.0')
        marker.set('style', 'overflow:visible')
        marker.set(inkex.addNS('stockid','inkscape'), name)

        arrow = inkex.etree.Element("path")
        arrow.set('d', 'M 0.0,0.0 L 5.0,-5.0 L -12.5,0.0 L 5.0,5.0 L 0.0,0.0 z ')
        if rotate:
            arrow.set('transform', 'scale(0.8) rotate(180) translate(12.5,0)')
        else:
            arrow.set('transform', 'scale(0.8) translate(12.5,0)')
        arrow.set('style', 'fill-rule:evenodd;stroke:#000000;stroke-width:1.0pt;marker-start:none')
        marker.append(arrow)

    def dimHLine(self, y, xlat):
        line = inkex.etree.Element("path")
        x1 = self.bbox[0] - xlat[0] * self.xoffset
        x2 = self.bbox[1]
        y = y - xlat[1] * self.yoffset
        line.set('d', 'M %f %f H %f' % (x1, y, x2))
        return line

    def dimVLine(self, x, xlat):
        line = inkex.etree.Element("path")
        x = x - xlat[0] * self.xoffset
        y1 = self.bbox[2] - xlat[1] * self.yoffset
        y2 = self.bbox[3]
        line.set('d', 'M %f %f V %f' % (x, y1, y2))
        return line

    def selection_bbox(self, svg_file=None, selected=None, scale=None):
        """Compute bbox of selected elements in initial viewport."""
        if svg_file is None:
            svg_file = self.svg_file
        if selected is None:
            selected = dict(self.selected)
        if scale is None:
            scale = self.unittouu('1px')
        bbox = None
        if self.options.type == "geometric":
            # Simpletransform for geometric bbox
            for node in selected.values():
                mat = nodeToRootTransform(node.getparent())
                bbox_root = computeBBox([node], mat)
                bbox = boxunion(bbox, bbox_root)
                if self.options.debug:
                    if bbox_root is not None and len(bbox_root) == 4:
                        draw_bbox(bbox_root, self.document.getroot())
        elif self.options.type == "visual":
            # Query inkscape for visual position and size
            for node_id in selected.keys():
                geom = query_bbox(svg_file, node_id, scale)
                if geom is not None and len(geom) == 4:
                    # combined SVG origin/viewport offset
                    if hasattr(inkex, 'get_root_offset'):
                        pos = inkex.get_root_offset(self.document)
                        geom[0] += pos[0]
                        geom[1] += pos[1]
                    # xmin, xMax, ymin, yMax
                    bbox_root = (geom[0], geom[0]+geom[2], geom[1], geom[1]+geom[3])
                    bbox = boxunion(bbox, bbox_root)
                    if self.options.debug:
                        draw_bbox(bbox_root, self.document.getroot())
        return bbox

    def effect(self):
        # Document scale
        scale = self.unittouu('1px')

        # Offset: convert desktop CSS px to user units
        self.xoffset = scale*self.options.xoffset
        self.yoffset = scale*self.options.yoffset

        # Check selection
        if len(self.options.ids) == 0:
            inkex.errormsg(_("Please select an object."))
            exit()

        # Bounding box relative to initial viewport (root)
        self.bbox = self.selection_bbox()

        # Avoid ugly failure on rects and texts.
        try:
            testing_the_water = self.bbox[0]
        except TypeError:
            inkex.errormsg(_('Unable to process this object.  Try changing it into a path first.'))
            exit()

        self.addMarker('Arrow1Lstart', False)
        self.addMarker('Arrow1Lend',  True)

        group = inkex.etree.Element(inkex.addNS('g', 'svg'))
        group.set('fill', 'none')
        group.set('stroke', 'black')

        line = self.dimHLine(self.bbox[2], [0, 1])
        line.set('marker-start', 'url(#Arrow1Lstart)')
        line.set('marker-end', 'url(#Arrow1Lend)')
        line.set('stroke-width', str(scale))
        group.append(line)

        line = self.dimVLine(self.bbox[0], [0, 2])
        line.set('stroke-width', str(0.5*scale))
        group.append(line)
        
        line = self.dimVLine(self.bbox[1], [0, 2])
        line.set('stroke-width', str(0.5*scale))
        group.append(line)
        
        line = self.dimVLine(self.bbox[0], [1, 0])
        line.set('marker-start', 'url(#Arrow1Lstart)')
        line.set('marker-end', 'url(#Arrow1Lend)')
        line.set('stroke-width', str(scale))
        group.append(line)
        
        line = self.dimHLine(self.bbox[2], [2, 0])
        line.set('stroke-width', str(0.5*scale))
        group.append(line)

        line = self.dimHLine(self.bbox[3], [2, 0])
        line.set('stroke-width', str(0.5*scale))
        group.append(line)

        # Append dimensions group to current layer
        layer = self.current_layer
        applyTransformToNode(rootToNodeTransform(layer), group)
        layer.append(group)

        if self.options.wrap_in_group:
            for node in self.selected.values():
                mat = fromToTransform(node.getparent(), group)
                applyTransformToNode(mat, node)
                group.append(node)
        

if __name__ == '__main__':
    e = Dimension()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
