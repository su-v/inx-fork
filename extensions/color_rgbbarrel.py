#!/usr/bin/env python

# local library
import coloreffect


class C(coloreffect.ColorEffect):
  def colmod(self,r,g,b):
    return '%02x%02x%02x' % (b,r,g)

c = C()
c.affect()
